Structured Questions Worker for Automated Assessment Platform
---

This repository stores the code that is used for the backend worker processing structured questions in AASP. 

# How it works
The base flow of how the system works is as follows:
1. Wait for jobs from the Redis Queue
2. Get relevant data from DB and save to Redis temporarily
3. If keyword is not case sensitive, convert submission and keyword to lowercase
4. Process the keywords by matching with submissions through Regex checks
5. Update score for the question
6. Update the submission worker by pushing to its Redis Queue


# Configuration
The following are the configuration options for the worker. You configure this through an environment variable or by using a .env file

## MySQL Database
| Enviroment Variable | Usage | Required | Defaults |
|---|---|---|---|
| MYSQL_HOST | IP Address of the MySQL Database | :x: | localhost |
| MYSQL_PORT | Port Number of the MySQL Database | :x: | 3306 |
| MYSQL_USER | MySQL User to be used by the worker | :white_check_mark: |  |
| MYSQL_PASSWORD | MySQL User Password | :white_check_mark: |  |
| MYSQL_DATABASE | MySQL Database used to store data from the worker | :x: | aasp |

## Redis Cache
Note that for redis, we also make use of a redis.conf for some of the configuration
| Enviroment Variable | Usage | Required | Defaults |
|---|---|---|---|
| REDIS_HOST | IP Address of the Redis Cache | :x: | localhost |
| REDIS_PORT | Port Number of the Redis Cache | :x: | 6379 |
| REDIS_PASSWORD | Password to access the Redis Cache | :white_check_mark: |  |

## Miscellaneous
| Enviroment Variable | Usage | Required | Defaults |
|---|---|---|---|
| DEBUG | If DEBUG mode is enabled | :x: | 0 |
| JOB_LIMIT | Maximum amount of jobs that the worker can handle at a time | :x: | 100 |
| TIMEOUT | Timeout while awaiting for job requests from the queue (seconds) | :x: | 3600 |
| QUEUE | Redis Queue used to queue structured questions attempts | :x: | structuredqueue |
| QUEUE_SUBMISSION | Redis Queue used to push back to submissions after the question is processed | :x: | submissionqueue |


# Development

To develop, ensure that the following software is installed in your development machine.

* [Docker Engine](https://docs.docker.com/engine/install/)
* [Golang](https://golang.org/)

Afterwhich, launch basic services with the worker
```bash
cd <worker location>
docker-compose -f docker-compose.yml up
```

Next, install the required dependencies for the worker with
```bash
go mod download
```

Finally start the worker in development mode by running
```bash
go build
./worker-structured
```

# Building Image
Image can be built by making use of [Docker Engine](https://docs.docker.com/engine/install/) so ensure that you have it installed. After, run the following commands
```bash
# Replace $IMAGE_TAG with an appropriate docker image tag 
docker build --pull -t $IMAGE_TAG .
```

You can also push the image up to a Docker Image Registry after with
```bash
# Replace $IMAGE_TAG with an appropriate docker image tag 
docker build --pull -t $IMAGE_TAG .
```

