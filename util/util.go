package util

import (
	"os"
	"strconv"
)

// Get string env variable, default to a string if not exists
func GetEnvDefault(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

// Get integer env variable, default to an integer if not exists
func GetEnvDefaultInt(key string, fallback int) int {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	val, err := strconv.Atoi(value)
	if err != nil {
		return fallback
	}
	return val
}
