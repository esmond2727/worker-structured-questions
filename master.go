package main

import (
	"encoding/json"
	"regexp"
	"strings"
	"sync"

	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kennethsohyq/school/university/fyp/worker-structured-questions/v2/models"
	"gitlab.com/kennethsohyq/school/university/fyp/worker-structured-questions/v2/util"
)

var counter int

func main() {
	err := godotenv.Load() // Check for environment file
	if err != nil {
		log.Println(".env file not found")
	}

	if util.GetEnvDefaultInt("DEBUG", 0) == 1 {
		log.SetLevel(log.DebugLevel)
		log.Debugln("DEBUG MODE ENABLED")
	}

	redisClient := util.SetupRedisClient()

	log.Println("Setting up redis client, ready for new jobs")
	var wg sync.WaitGroup
	maxJobs := util.GetEnvDefaultInt("JOB_LIMIT", 100)
	timeout := util.GetEnvDefaultInt("TIMEOUT", 3600) // Timeout in seconds
	log.Println("Max Jobs for Worker:", maxJobs, "| Timeout:", timeout, "seconds")

	counter = 0

	for {
		// Only allow certain number of simultaneous jobs
		if counter >= maxJobs {
			log.Println("Waiting for active jobs to finish...")
			wg.Wait()
			counter = 0
		}

		// Blocking pop to await for jobs
		res, err := redisClient.BLPop(util.BgCtx, 0, util.GetEnvDefault("QUEUE", "structuredqueue")).Result()
		if err != nil {
			panic(err)
		}
		log.Debugln(res[1])

		// Process the json
		var input models.QueueObj
		err3 := json.Unmarshal([]byte(res[1]), &input)
		if err3 != nil {
			log.Errorln(err3)
			log.Warnln("Ignoring...")
			continue
		}

		// Get relevant data out from DB from key
		// TODO: Reimplement

		data, keywords := util.GetSubmissionAttemptAsRedis(input.Key)
		if data == nil || keywords == nil {
			log.Warnln("THERE ARE EITHER NO SUBMISSION OR KEYWORDS FOR", input.Key)
			updateSubmissionAttempt(redisClient, input.Key, 0)
			continue
		}
		log.Debugln(keywords)
		log.Debugln(data)
		// Process the submission
		//// Save data to redis for update
		wg.Add(1)
		counter++
		go goProcessSubmission(&wg, data, keywords, input.Key, redisClient)
	}
}

// Push to the submission queue for the submission worker to pick up and validate marked state
func updateSubmissionAttempt(client *redis.Client, aid string, score float64) {
	// Update MySQL DB
	util.UpdateSubmissionAttempt(aid, score)
	log.Println("Submission for Attempt #", aid, "updated successfully")
	client.RPush(util.BgCtx, util.GetEnvDefault("QUEUE_SUBMISSION", "submissionqueue"), "c-"+aid) // Push a check key back to submission queue
}

// Concurrently process submissions
func goProcessSubmission(wg *sync.WaitGroup, answer *models.Answer, keywords []models.Keywords, key string, client *redis.Client) {
	defer wg.Done()

	totalScore := 0.0

	for _, keyword := range keywords {
		cmpKeyword := keyword.Word
		cmpAns := answer.Response
		if !keyword.CaseSensitive {
			// Use lowered if not case sensitive
			cmpKeyword = strings.ToLower(cmpKeyword)
			cmpAns = answer.ResponseLowered
		}

		// Use regex to compare word
		regExpObj := regexp.MustCompile(`\b(` + cmpKeyword + `)\b`)
		log.Debugf("Comparing regex exp: %s | Word: %s\n", regExpObj.String(), cmpKeyword)

		matches := regExpObj.FindAllString(cmpAns, -1)
		log.Debugln(matches)
		matchCnt := len(matches)
		matchScore := 0.0
		score, err := keyword.Score.Float64()
		if err != nil {
			log.Errorln("Error getting score")
			score = 0.0
		}
		if matchCnt > keyword.Occurrence {
			// Only occurrence
			matchScore += float64(keyword.Occurrence) * score
		} else {
			matchScore += float64(matchCnt) * score
		}
		log.Debugf("Found number of matches: %d | Occurance to count: %d | Total Score For Word: %f\n", matchCnt, keyword.Occurrence, matchScore)
		totalScore += matchScore
	}
	ms, _ := answer.MaxScoreQn.Float64()
	if totalScore > ms {
		totalScore = ms
	}
	log.Debugf("Final Score for question: %f", totalScore)
	updateSubmissionAttempt(client, key, totalScore)
}
