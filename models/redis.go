package models

import "encoding/json"

type QueueObj struct {
	Key  string `json:"key"`
	Type string `json:"type"`
}

type Answer struct {
	Response        string      `json:"response"`
	ResponseLowered string      `json:"response_lowercase"`
	MaxScoreQn      json.Number `json:"max_score"`
}

type Keywords struct {
	Word          string      `json:"keyword"`
	Occurrence    int         `json:"occurance"`
	Score         json.Number `json:"score"`
	CaseSensitive bool        `json:"CaseSensitive"`
}
