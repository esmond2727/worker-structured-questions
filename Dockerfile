FROM golang:latest AS build

WORKDIR /go/src/app
COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build

FROM alpine:latest

RUN apk add --no-cache libc6-compat

WORKDIR /
COPY --from=build /go/src/app/worker-structured-questions /worker-structured

CMD ["./worker-structured"]